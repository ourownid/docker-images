# Super User LTD Docker Images

We will try to provide some images that we are using on our current work. We think they might be useful for the community.

  - CentOS 7 (latest) with OpenJDK 8 (CentOS 7 update package installation)

> We provide every image "as is". You can use it at your own risk. We are not responsible for its use nor provide any support on it, but if you find it useful, we are glad to receive comments and/or how to enhance them.


### How to build it
You can get any image for our git repository, and build it using docker build -t *name* *image-folder*

